gen-proto:
	# protoc proto/*.proto --go_out=. --go_opt=paths=source_relative --proto_path=.
	protoc --go_out=. --go_opt=paths=source_relative --go-grpc_out=. --go-grpc_opt=paths=source_relative proto/*.proto

run-server:
	go run server/server.go

gen-api:
	protoc -I . --go_out . --go_opt=paths=source_relative --go-grpc_out . --go-grpc_opt paths=source_relative server/api/*.proto
	protoc -I . --grpc-gateway_out . --grpc-gateway_opt logtostderr=true --grpc-gateway_opt paths=source_relative --grpc-gateway_opt generate_unbound_methods=true server/api/*.proto

gen-swagger:
	protoc -I . --openapiv2_out ./server/swagger --openapiv2_opt logtostderr=true server/api/*.proto