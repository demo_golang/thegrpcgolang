package database

import (
	"context"
	"os"
	"time"

	"gitlab.com/demo_golang/thegrpcgolang/server/helper"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func OpenDatabaseMongo() (*mongo.Database, *context.Context) {
	helper.LoadENV()
	mongo_url := os.Getenv("MONGODB_URL")
	mongo_dbName := os.Getenv("MONGODB_DBNAME")
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	clientOptions := options.Client().ApplyURI(mongo_url)
	client, _ := mongo.Connect(ctx, clientOptions)
	return client.Database(mongo_dbName), &ctx
}
