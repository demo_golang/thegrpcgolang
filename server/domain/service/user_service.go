package service

import (
	"fmt"

	"gitlab.com/demo_golang/thegrpcgolang/server/domain/entities"
	"gitlab.com/demo_golang/thegrpcgolang/server/infrastructure/repository"
)

type userService struct {
	userRepository repository.UserRepository
}

type UserService interface {
	InsertUser(user entities.UserInfo) entities.UserInfo
	GetAllUser() []entities.UserInfo
}

func NewUserService(userRepo repository.UserRepository) UserService {
	return &userService{
		userRepository: userRepo,
	}
}

func (u *userService) GetAllUser() []entities.UserInfo {
	listUser := *u.userRepository.GetAll()
	if len(listUser) == 0 {
		fmt.Print("không có dữ liệu")
	}

	return listUser
}

func (u *userService) InsertUser(user entities.UserInfo) entities.UserInfo {
	_user := u.userRepository.Insert(user)
	return *_user
}
