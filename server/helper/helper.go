package helper

import (
	"fmt"
	"log"
	"strings"

	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func LoadENV() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
		panic("Error loading .env file")
	}
}

func GenObjectID(key ...string) (primitive.ObjectID, error) {
	h := fmt.Sprintf("%x", strings.Join(key, ""))
	if len(h) > 24 {
		h = h[:24]
	}

	return primitive.ObjectIDFromHex(h)
}
