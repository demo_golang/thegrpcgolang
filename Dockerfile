FROM golang:1.18.0-alpine3.15 AS builder
EXPOSE 5004
EXPOSE 8081
# # Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux \
    GOARCH=amd64

# Move to working directory /build
WORKDIR /build

# COPY go.* .
# # Copy the code into the container
COPY . .

RUN go install \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
    google.golang.org/protobuf/cmd/protoc-gen-go \
    google.golang.org/grpc/cmd/protoc-gen-go-grpc


# Copy and download dependency using go mod

RUN go mod download
RUN go mod tidy

# Build the application
RUN go build -a -tags netgo -ldflags '-w' -o main .

# Move to /dist directory as the place for resulting binary folder
WORKDIR /app

# Copy binary from build to main folder
RUN cp /build/main .

# Build a small image
FROM scratch

COPY --from=builder /app/main /
# COPY ./data .
# COPY ./index.html .
ADD .env .
# Command to run
ENTRYPOINT ["/main"]

# FROM golang:alpine
# EXPOSE 4500
# WORKDIR /build
# COPY . .
# RUN go build -o main .
# WORKDIR /app
# # Copy binary from build to main folder
# RUN cp /build/main .
# RUN mkdir /data
# CMD ["/main"]