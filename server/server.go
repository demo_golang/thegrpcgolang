package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.com/demo_golang/thegrpcgolang/server/api"
	gw "gitlab.com/demo_golang/thegrpcgolang/server/api"
	"gitlab.com/demo_golang/thegrpcgolang/server/controller"
	"gitlab.com/demo_golang/thegrpcgolang/server/domain/service"
	"gitlab.com/demo_golang/thegrpcgolang/server/infrastructure/database"
	"gitlab.com/demo_golang/thegrpcgolang/server/infrastructure/repository"
	"google.golang.org/grpc"
)

const address = "localhost:5004"

// var (
// 	// command-line options:
// 	// gRPC server endpoint
// 	grpcServerEndpoint = flag.String("grpc-server-endpoint", "localhost:5004", "gRPC server endpoint")
// )

// func run() error {
// 	ctx := context.Background()
// 	ctx, cancel := context.WithCancel(ctx)
// 	defer cancel()

// 	// Register gRPC server endpoint
// 	// Note: Make sure the gRPC server is running properly and accessible
// 	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
// 	if err != nil {
// 		return err
// 	}

// 	// Start HTTP server (and proxy calls to gRPC server endpoint)
// }

var (
	dbMongo, ctx                              = database.OpenDatabaseMongo()
	userRepository repository.UserRepository  = repository.NewUserRepository(dbMongo, ctx)
	userService    service.UserService        = service.NewUserService(userRepository)
	userController controller.UserServiceGrpc = controller.NewUserController(userService)
)

func main() {
	go func() {
		mux := runtime.NewServeMux()
		gw.RegisterUserServiceHandlerServer(context.Background(), mux, &userController)
		log.Fatal(http.ListenAndServe(":8081", mux))
	}()

	conn, err := net.Listen("tcp", address)

	if err != nil {
		log.Fatal("tcp connect erro: ", err.Error())
	}

	grpcServer := grpc.NewServer()

	server := controller.UserServiceGrpc{}

	api.RegisterUserServiceServer(grpcServer, &server)

	fmt.Println("Starting gRPC server at: ", address)

	err = grpcServer.Serve(conn)
	if err != nil {
		log.Fatal(err)
	}

}
