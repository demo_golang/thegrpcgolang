package controller

import (
	"context"

	"gitlab.com/demo_golang/thegrpcgolang/server/api"
	"gitlab.com/demo_golang/thegrpcgolang/server/domain/entities"
	"gitlab.com/demo_golang/thegrpcgolang/server/domain/service"
	"gitlab.com/demo_golang/thegrpcgolang/server/helper"
)

type UserServiceGrpc struct {
	api.UnimplementedUserServiceServer
	userService service.UserService
}

func NewUserController(userService service.UserService) UserServiceGrpc {
	return UserServiceGrpc{
		userService: userService,
	}
}

func (s UserServiceGrpc) GetTest(ctx context.Context, req *api.InputText) (*api.InputText, error) {
	return req, nil
}

func (s UserServiceGrpc) InsertUser(ctx context.Context, req *api.InsertUserDTO) (*api.UserResponse, error) {
	_params := entities.UserInfo{}
	_params.Username = req.UserName
	_params.Password = req.Password
	_params.Email = req.Email
	_params.Avatar = req.Avatar
	_params.SoDienThoai = req.SoDienThoai
	_params.Id, _ = helper.GenObjectID(req.UserName, req.SoDienThoai)
	_data := s.userService.InsertUser(_params)

	_item := api.UserResponse{}
	_item.Id = _data.Id.Hex()
	_item.UserName = _data.Username
	_item.Password = _data.Password
	_item.SoDienThoai = _data.SoDienThoai
	_item.Email = _data.Email
	_item.FullName = _data.FullName

	return &_item, nil
}
