package repository

import (
	"context"
	"log"

	"gitlab.com/demo_golang/thegrpcgolang/server/domain/entities"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type connection struct {
	mongoDb *mongo.Database
}

type UserRepository interface {
	Insert(user entities.UserInfo) *entities.UserInfo
	GetAll() *[]entities.UserInfo
}

func NewUserRepository(mongo *mongo.Database, ctx *context.Context) UserRepository {
	return &connection{
		mongoDb: mongo,
	}
}

func (db *connection) GetAll() *[]entities.UserInfo {

	var user_info []entities.UserInfo

	collection := db.mongoDb.Collection("user_info")
	cursor, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		log.Fatal(err)
	}

	defer cursor.Close(context.TODO())
	for cursor.Next(context.TODO()) {
		var user entities.UserInfo
		cursor.Decode(&user)
		user_info = append(user_info, user)
	}

	return &user_info
}

func (db *connection) Insert(user entities.UserInfo) *entities.UserInfo {
	opt := options.UpdateOptions{}
	opt.SetUpsert(true)
	collection := db.mongoDb.Collection("user_info")
	pByte, err := bson.Marshal(user)
	if err != nil {
		log.Fatal(err)
	}

	var update bson.M
	err = bson.Unmarshal(pByte, &update)
	if err != nil {
		log.Fatal(err)
	}

	_, err = collection.UpdateOne(context.Background(), bson.M{"_id": user.Id}, bson.D{{Key: "$set", Value: update}}, &opt)
	if err != nil {
		log.Fatal(err)
	}

	return &user
}
