package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type UserInfo struct {
	Id          primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Username    string             `json:"userName" bson:"userName"`
	Password    string             `json:"password" bson:"password"`
	FullName    string             `json:"fullName" bson:"fullName"`
	Avatar      string             `json:"avatar" bson:"avatar"`
	SoDienThoai string             `json:"soDienThoai" bson:"soDienThoai"`
	Email       string             `json:"email" bson:"email"`
}
